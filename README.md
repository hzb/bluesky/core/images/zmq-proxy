### An Image for Running a Bluesky 0MQ Buffer

Start a proxy on port 557 and 5578 with:

`docker run -d --rm --name bluesky_zmq --network host registry.hzdr.de/hzb/bluesky/core/images/zmq-proxy:1-0-0 bluesky-0MQ-proxy 5577 5578`